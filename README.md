This is a land rover based vehicle simulator made in gazebo with ROS capabilities.

Currently we added support for the six wheels controllers, rviz visualization and basic sensors (Kinect camera in front, LIDAR and thermal camera).

For running it is necessary install hector_sensors_description and hector_quadrotor_demo packages for ROS Kinetic because of the used sensors and scenarios.

Type the following command for start simulation in gazebo and rviz:

* roslaunch rover_gearbox roverx_gazebo.launch

Other features for path planning and motion are still in development.


Authors:

Ariel E. Ortiz Esquivel. arieleo@ccc.inaoep.mx
Francisco Zarate Quintero. fzq.032@gmail.com